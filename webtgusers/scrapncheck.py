#!/usr/bin/env python3

import logging
import sys
import requests

from bs4 import BeautifulSoup
from time import sleep

def load_users(filename):
    with open(filename, 'r') as f:
        users = [ r.strip() for r in f.readlines() ]
    return users


# Make an HTTP Request
target_url = 'https://crawler-test.com/'
response_data = requests.get(target_url)

# Parse the HTML content
soup = BeautifulSoup(response_data.text, 'html.parser')

def check_tg_user(username):
    target_url = f"https://t.me/{username}"
    response_data = requests.get(target_url)

    if not response_data.ok:
        logging.warn(f"Query user: {username}, response: {response.status_code}")
        return False

    # Parse the HTML content
    soup = BeautifulSoup(response_data.text, 'html.parser')

    if soup.find('div', class_='tgme_page_extra') is None:
        return False
    
    return True

def main():
    # Make an HTTP Request
    users = load_users(sys.argv[1])
    position = 0
    if len(sys.argv) > 2:
        startafter = sys.argv[2]
        position = users.index(startafter) + 1
        print(f"start from user {users[position]}")

    processed_list = users[position:]

    try: 
        for u in processed_list:
            if check_tg_user(u):
                print(f"{u} OK")
            else:
                print(f"{u} Fail")
                sleep(0.5)
    except KeyboardInterrupt:
        print("[CTRL+C detected]")


if __name__ == '__main__':
    main()
