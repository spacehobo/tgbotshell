# tgbotshell

Python-based Telegram -Bot- Interactive Shell. Initially intended to learn Telegram Bot APi, but later switched to MTProto.

## Links

Telegram MTProto API Framework for Python: https://docs.pyrogram.org/
    https://pypi.org/project/Pyrogram/

Android SMS gateway: https://github.com/capcom6/android-sms-gateway
Considered to use for automating OTP authentication. 
