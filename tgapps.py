import asyncio
import importlib
import logging
import os
import pickle
import shutil
import yaml

from datetime import datetime

from pyrogram import Client
from pyrogram.errors import BadRequest, FloodWait

__version__ = '0.0.2'

logging.basicConfig(level=logging.INFO)

FLOOD_DETECTED = '[420 FLOOD_WAIT_X]'

# Replace these with your API ID and hash
class AppConfig:
    def __init__(self):
        self.API_ID = os.getenv('API_ID', '')
        self.API_HASH = os.getenv('API_HASH', '')
        self.PHONE_NUMBER = os.getenv('PHONE_NUMBER', '')
        self.SESSION_NAME = os.getenv('SESSION_NAME', '')

    def loadfile(self, fname="app-settings.yaml"):
        with open(fname, "r") as f:
            conf = yaml.safe_load(f)
        app, session = conf['application'], conf['session']
        self.API_ID = app['api_id']
        self.API_HASH = app['api_hash']
        self.PHONE_NUMBER = session['phone_number']
        self.SESSION_NAME = session['session_name']


# assert None not in [API_ID, API_HASH, PHONE_NUMBER], "All env variables of API_ID, API_HASH, PHONE_NUMBER should be defined"

# Create the client
# app = Client("my_account", api_id=api_id, api_hash=api_hash)

# BACKUP_SUFFIX_FORMAT = "%Y%m%d%H%M%S"
BACKUP_SUFFIX_FORMAT = "%m%d%H%M%S"

class PklStorage:
    def __init__(self, fname):
        self.filename = fname.strip()
    def load(self):
        with open(self.filename, "br") as f:
            data = pickle.load(f)
        return data
    def dump(self, data, backup=True):
        if backup:
            self.make_backup()
        with open(self.filename, "bw") as f:
            pickle.dump(data, f)
    def make_backup(self):
        if os.path.isfile(self.filename):
            suffix = datetime.now().strftime(BACKUP_SUFFIX_FORMAT)
            bckpfname = f"{self.filename}.{suffix}"
            shutil.copy(self.filename, bckpfname)
        else:
            logging.warn(f"file {self.filename} does not exist. The backup skipped")
    def append(self, data):
        stored = self.load()
        stored.extend(data)
        print(stored)
        self.dump(stored)

class Application:
    def __init__(self, c):
        api_id = c.API_ID if isinstance(c.API_ID, int) else int(c.API_ID, 10)
        self.app = Client(c.SESSION_NAME, api_id=api_id, api_hash=c.API_HASH, phone_number=c.PHONE_NUMBER)
        print("Client created: ", c.SESSION_NAME, c.PHONE_NUMBER)

    async def start(self):
        await self.app.start()

    async def stop(self):
        await self.app.stop()

    def get_user_id(self, username):
        # with self.app:
        try:
            user = self.app.get_users(username)
            print("User ID:", user.id)
        except Exception as e:
            print(f"Error occurred for getting data for user {username}: {e}")
        return user

    async def get_chat_members(self, chatid) -> list:
        return [ m async for m in self.app.get_chat_members(chatid) ]

    async def add_users(self, chid, userlist, stoplist, count, interval=5):
        from time import sleep

        delay = 0
        ul_filtered = list(filter(lambda el: el not in stoplist, userlist))
        for usrs in iterby(ul_filtered, count):
            usernames = [ u for u in usrs if u is not None ]
            try:
                ok = await self.app.add_chat_members(chid, usernames)
                isok = "ok" if ok else "fail"
                logging.info(f"{usernames} - {isok}")
            except FloodWait as e:
                delay = e.value
                logging.warning(f"{usernames}: Blocked by a flood detector. Await for {delay}s")
            except BadRequest as e:
                logging.warning(f"{usernames}: BadRequest detected: {e}")
            except Exception as e:
                logging.info(f"{usernames} - fail with exception: {e}")
            await asyncio.sleep(interval+delay)
            delay = 0

def iterby(alist, count):
    i = None
    for i in range(len(alist)//count):
        yield alist[i*count:i*count+count]
    i, leftover = 0 if i is None else i + 1, len(alist) % count
    if leftover != 0:
        yield alist[i*count:i*count+leftover]

def load_users(filename):
    with open(filename, 'r') as f:
        users = [ r.strip() for r in f.readlines() ]
    return users

def reload_mod(m):
    importlib.reload(m)

def main():
    pass

if __name__ == '__main__':
    main()
