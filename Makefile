VERSION=0.0.1

venv:
	python3 -m venv venv
	( . venv/bin/activate; pip install -r requirements.txt )

